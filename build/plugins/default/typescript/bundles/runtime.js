(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.componentClassName = "Sup.Behavior";
function setupComponent(player, component, config) {
    if (config.propertyValues == null)
        return;
    var behaviorInfo = player.resources.behaviorProperties.behaviors[config.behaviorName];
    for (var name_1 in config.propertyValues) {
        var valueInfo = config.propertyValues[name_1];
        var ancestorBehaviorInfo = behaviorInfo;
        var behaviorPropertyInfo = void 0;
        while (ancestorBehaviorInfo != null) {
            behaviorPropertyInfo = ancestorBehaviorInfo.propertiesByName[name_1];
            if (behaviorPropertyInfo != null)
                break;
            ancestorBehaviorInfo = player.resources.behaviorProperties.behaviors[ancestorBehaviorInfo.parentBehavior];
        }
        if (behaviorPropertyInfo == null) {
            console.warn("Tried to set a property named " + name_1 + " on behavior class " + component.__outer.constructor.name + " " +
                "but no such property is declared. Skipping.");
            continue;
        }
        if (behaviorPropertyInfo.type !== valueInfo.type) {
            console.warn("Tried to set a value of type " + valueInfo.type + " for property " + component.__outer.constructor.name + "." + name_1 + " " +
                ("but property type is declared as " + behaviorPropertyInfo.type + ". Skipping."));
            continue;
        }
        // Convert value based on type
        switch (behaviorPropertyInfo.type) {
            case "Sup.Math.Vector2":
                {
                    component.__outer[name_1] = new window.Sup.Math.Vector2(valueInfo.value.x, valueInfo.value.y);
                }
                break;
            case "Sup.Math.Vector3":
                {
                    component.__outer[name_1] = new window.Sup.Math.Vector3(valueInfo.value.x, valueInfo.value.y, valueInfo.value.z);
                }
                break;
            default:
                {
                    component.__outer[name_1] = valueInfo.value;
                }
                break;
        }
    }
}
exports.setupComponent = setupComponent;

},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function loadResource(player, resourceName, callback) {
    player.getAssetData("resources/" + resourceName + "/resource.json", "json", function (err, data) {
        if (err != null) {
            callback(err);
            return;
        }
        for (var behaviorName in data.behaviors) {
            var behavior = data.behaviors[behaviorName];
            behavior.propertiesByName = {};
            for (var _i = 0, _a = behavior.properties; _i < _a.length; _i++) {
                var property = _a[_i];
                behavior.propertiesByName[property.name] = property;
            }
        }
        callback(null, data);
    });
}
exports.loadResource = loadResource;

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var script = require("./script");
var Behavior = require("./Behavior");
var behaviorProperties = require("./behaviorProperties");
SupRuntime.registerPlugin("script", script);
SupRuntime.registerPlugin("Behavior", Behavior);
SupRuntime.registerResource("behaviorProperties", behaviorProperties);

},{"./Behavior":1,"./behaviorProperties":2,"./script":4}],4:[function(require,module,exports){
"use strict";
/// <reference path="../../typescript/typescriptAPI/TypeScriptAPIPlugin.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var actorComponentTypesByName = {};
function init(player, callback) {
    player.behaviorClasses = {};
    player.createActor = function (name, parentActor, options) {
        return new window.Sup.Actor(name, parentActor, options);
    };
    player.createComponent = function (type, actor, config) {
        if (type === "Behavior") {
            var behaviorClass = player.behaviorClasses[config.behaviorName];
            if (behaviorClass == null) {
                throw new Error("Could not find a behavior class named \"" + config.behaviorName + "\" for actor \"" + actor.getName() + "\". " +
                    "Make sure you're using the class name, not the script's name and that the class is declared " +
                    "before the behavior component is created (or before the scene is loaded).");
            }
            return new behaviorClass(actor.__inner);
        }
        else {
            if (actorComponentTypesByName[type] == null) {
                actorComponentTypesByName[type] = window;
                var parts = SupRuntime.plugins[type].componentClassName.split(".");
                for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
                    var part = parts_1[_i];
                    actorComponentTypesByName[type] = actorComponentTypesByName[type][part];
                }
            }
            return new actorComponentTypesByName[type](actor);
        }
    };
    callback();
}
exports.init = init;
function start(player, callback) {
    player.getAssetData("script.js", "text", function (err, script) {
        var scriptFunction = new Function("_player", script);
        scriptFunction(player);
        callback();
    });
}
exports.start = start;

},{}]},{},[3]);
