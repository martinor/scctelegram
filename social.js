var social = {
	isLoggedIn: function () {
		return true
	},
	submitScore: function (score, callback, params) {
		var req = new XMLHttpRequest()
		req.open('POST', '/score', true)
		req.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
		req.send('{"score": ' + score + '}')
		callback()
	},
	shareScore: function () {
		if (!window.TelegramGameProxy) {
			return console.log("No se encuentra TelegramGameProxy")
		}
		window.TelegramGameProxy.shareScore()
	}
}

window.social = social
