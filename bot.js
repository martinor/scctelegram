require('dotenv').config()
const { Composer, Markup } = require('micro-bot')
const finalHandler = require('finalhandler')
const Deploy = require('./deploy')
const jws = require('jws')

const gameShortName = 'scc'
const initMessage = `
*Super Coin Collector*
Pulsa "Juega con amigos", luego selecciona a quien quieres retar.
O prueba tu habilidad en solitario con el boton "Juega ahora".
`
const inlineAnswer = [{
	type: 'game',
	id: 'scc',
	game_short_name: 'scc',
	reply_markup: Markup.inlineKeyboard([
		Markup.gameButton('¡Juega ahora!'),
		Markup.urlButton('Version completa', 'https://play.google.com/store/apps/details?id=system4.games.superCoinCollector')
	], {columns: 1})
}]
const extra = Markup.inlineKeyboard([
	Markup.switchToCurrentChatButton('¡Juega ahora!', ''),
	Markup.switchToChatButton('Juega con amigos', ''),
	Markup.urlButton('Version completa', 'https://play.google.com/store/apps/details?id=system4.games.superCoinCollector')
], {columns: 1}).extra()
const bot = new Composer()

bot.gameQuery((ctx) => {
	const game = ctx.callbackQuery.game_short_name
	const token = jws.sign({
		header: { alg: 'HS512' },
		payload: {
			game: game,
			user: ctx.from.id,
			imessage: ctx.callbackQuery.inline_message_id,
			message: (ctx.callbackQuery.message || {}).message_id,
			chat: (ctx.chat || {}).id
		},
		secret: process.env.SIGN_SECRET
	})

	return ctx.answerCallbackQuery(null, `${process.env.EXTERNAL_DOMAIN}/?token=${token}`)
})
bot.on('inline_query', (ctx) => ctx.answerInlineQuery(inlineAnswer))
bot.on('text', (ctx) => ctx.replyWithMarkdown(initMessage, extra))
Deploy.init()

module.exports = {
	botHandler: bot,
	requestHandler: (req, res) => Deploy.router(req, res, finalHandler(req, res))
}
