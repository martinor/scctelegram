require('dotenv').config()
const finalHandler = require('finalhandler')
const serveStatic = require('serve-static')
const bodyParser = require('body-parser')
const { Telegram } = require('micro-bot')
const Connect = require('connect')
const Router = require('router')
const http = require('http')
const jws = require('jws')
const url = require('url')


var deploy = {
	score: null,
	app: Connect(),
	port: process.env.OPENSHIFT_NODEJS_PORT || 8080,
	router: Router(),
	baseDir: __dirname + '/build',
	telegram: new Telegram(process.env.BOT_TOKEN),
	handler: (req, res) => this.router(req, res, finalHandler(req, res)),
	init: function () {
		//this.router.use(serveStatic('build'))
		this.score = this.router.route('/score')
		this.score.all((req, res, next) => {
			const token = url.parse(req.headers.referer).query.slice(6)
			if(!jws.verify(token, 'HS512', process.env.SIGN_SECRET)) {
				res.statusCode = 403
				return res.end()
			}
			req.microGame = JSON.parse(jws.decode(token).payload)
			next()
		})
		this.score.all(bodyParser.json())
		this.score.get((req, res) => {
			const { user, imessage, chat, message } = req.microGame
			this.telegram.getGameHighScores(user, imessage, chat, message)
				.then((scores) => {
					res.setHeader('content-type', 'application/json')
					res.statusCode = 200
					res.end(JSON.stringify(scores, null, true))
				})
				.catch((err) => {
					console.log(err)
					res.statusCode = 500
					res.end()
				})
		})
		this.score.post((req, res) => {
			const scoreValue = parseInt(req.body.score)
			if (scoreValue <= 0) {
				res.statusCode = 200
				res.end()
			}
			const { user, imessage, chat, message } = req.microGame
			this.telegram.setGameScore(user, scoreValue, imessage, chat, message, true)
				.then(() => {
					res.statusCode = 200
					res.end()
				})
				.catch((err) => {
					res.statusCode = err.code || 500
					res.end(err.description)
				})
		})
		
		this.app.use(serveStatic(this.baseDir, {'index': ['index.html']}));
		this.app.use(function (req, res) {
			deploy.router(req, res, finalHandler(req, res))
		})
		this.app.listen(this.port)
	}
}

module.exports = deploy
